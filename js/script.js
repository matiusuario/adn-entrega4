//JavaScript
const sliderImage = document.getElementById("sliderImage");
let sufijo = "01";

function cambiarImagen() {
	const prefijo = "assets/img/scr";
	const extension = ".png";
	let ruta;

	if (sufijo < "18") {
		sufijo++;
	} else {
		sufijo = "02";
	}
	ruta = prefijo + sufijo.toString().padStart(2, "0") + extension;
	sliderImage.setAttribute("src", ruta);
	console.log("se ejecutó cambiarImagen y se estableció la ruta: ", ruta);
}

const interv = setInterval(cambiarImagen, 2000);
